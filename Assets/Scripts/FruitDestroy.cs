﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class FruitDestroy : MonoBehaviour
{
	[SerializeField] private GameObject[] _fragments;

	[SerializeField] private int _numberOfPointsForThisFruit;

	[Inject] private FruitsCount fruitsCount;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "Knife")
		{
			DestroyAndEnableFragments();
		}
	}

	public void DestroyAndEnableFragments()
	{
		Debug.Log("fruit");
		foreach (var fragment in _fragments)
		{
			fragment.SetActive(true);
			fragment.transform.SetParent(null);
		}

		fruitsCount.AddFruit(_numberOfPointsForThisFruit);

		Destroy(gameObject);
	}

	public class Factory : PlaceholderFactory<FruitDestroy>
	{
	}
}
