﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LevelInitializeController : BaseController
{
	public event Action NewStageCreated;
	public event Action StageFailedEvent;
	public event Action BossStageCreated;
	[field:SerializeField] public bool IsBossStage { get; private set; }
	[SerializeField] private GameSettings _gameSettings;
	[SerializeField] private Transform _targetStartPos;
	[SerializeField] private int _stageCount;
	[SerializeField] private int _maxNumberOfStages;//максимальное кол-во этапов до босса
	[SerializeField] private float _newStageTimer;
	private float _newStageTimerStartValue;
	[Inject] private CircleRotateController _rotateController;
	[Inject] private ShootController _shootController;
	[Inject] private DestroyTargetController _destroyTargetController;
	[Inject] private UiMessages _uiMessages;
	[Inject] private CreatedObject _createdObject;
	[SerializeField] private int _numberIfKnives;
	private float _targetRotateSpeed;
	[SerializeField] private bool _isNeedCreateNewStage;
	private bool _isStageFailed;

	private void Start()
	{
		_newStageTimerStartValue = _newStageTimer;
		_numberIfKnives = _gameSettings.GetStartNumberOfKnives();
		_targetRotateSpeed = _gameSettings.GetTargetStartRotateSpeed();
		_maxNumberOfStages = _gameSettings.GetMaxNumberOfStages();

		_destroyTargetController.TargetDestroyed += IsNeedCreateNewStageTrue;
		_uiMessages.RestartButtonClicked += RestartButtonClickedHandler;

		CreateStage(_targetRotateSpeed, _numberIfKnives, _gameSettings.GetStandartTarget());
	}

	private void OnDestroy()
	{
		_destroyTargetController.TargetDestroyed -= IsNeedCreateNewStageTrue;
		_uiMessages.RestartButtonClicked -= RestartButtonClickedHandler;
	}

	public override void ControllerUpdate()
	{
		if (_isNeedCreateNewStage)
		{
			_newStageTimer -= Time.deltaTime;

			if (_newStageTimer <= 0)
			{
				CreateNextStage();
				_newStageTimer = _newStageTimerStartValue;
				_isNeedCreateNewStage = false;
			}
		}

		StageFailed();
	}

	public void StageFailedTrue()
	{
		_isStageFailed = true;
	}

	private void StageFailed()
	{
		if (_isStageFailed)
		{
			_newStageTimer -= Time.deltaTime;
			StageFailedEvent?.Invoke();
			IsBossStage = false;

			if (_newStageTimer <= 0)
			{
				_numberIfKnives = _gameSettings.GetStartNumberOfKnives();
				_targetRotateSpeed = _gameSettings.GetTargetStartRotateSpeed();
				_destroyTargetController.DestroyTargetWithoutEffects();
				CreateStage(_targetRotateSpeed, _numberIfKnives, _gameSettings.GetStandartTarget());
				_stageCount = 1;
				NewStageCreated?.Invoke();
				_newStageTimer = _newStageTimerStartValue;
				_isStageFailed = false;
			}
		}
	}

	private void CreateNextStage()
	{

		if (_stageCount < _maxNumberOfStages)
		{
			CreateStage(_targetRotateSpeed += _gameSettings.GetHowMuchTheRotateSpeedWillIncreaseForTheCompletedStage(),
				_numberIfKnives += _gameSettings.GetHowManyKnivesWillBeAddedForTheCompletedStage(), _gameSettings.GetRandomTargetFromList());
		}
		else
		{
			CreateStage(_targetRotateSpeed += _gameSettings.GetHowMuchTheRotateSpeedWillIncreaseForTheCompletedStage(),
				_numberIfKnives += _gameSettings.GetHowManyKnivesWillBeAddedForTheCompletedStage(), _gameSettings.GetRandomBossFromList());
			BossStageCreated?.Invoke();
			IsBossStage = true;
			Debug.Log("bossStage");
		}
		NewStageCreated?.Invoke();
	}

	private void CreateStage(float targetRotateSpeed, int numberOfKnives, Target targetObj)
	{
		_rotateController.SetRotateSpeed(targetRotateSpeed);
		_shootController.SetNumberOfKnives(numberOfKnives);
		var target = Instantiate(targetObj, _targetStartPos.position, Quaternion.identity);
		_createdObject.CreatedObjectSetParent(target);
		_rotateController.SetTatget(target);
		_destroyTargetController.SetTarget(target);
		_stageCount++;
	}

	private void IsNeedCreateNewStageTrue()
	{
		if(!IsBossStage)
		_isNeedCreateNewStage = true;
	}

	private void RestartButtonClickedHandler()
	{
		CreateStage(_targetRotateSpeed, _numberIfKnives, _gameSettings.GetStandartTarget());
		NewStageCreated?.Invoke();
		IsBossStage = false;
	}
}
