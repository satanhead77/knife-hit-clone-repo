﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleRotateController : BaseController
{
	[SerializeField] private Target _target;
	[SerializeField] private float _rotateSpeed;

	public override void ControllerUpdate()
	{
		if (_controllerIsEnabked)
		{
			Rotate();
		}
	}

	public void SetRotateSpeed(float rotateSpeed)
	{
		_rotateSpeed = rotateSpeed;
	}

	public void SetTatget(Target target)
	{
		_target = target;
	}

	private void Rotate()
	{
		if(_target!=null)
		_target.transform.Rotate(0, 0, -_rotateSpeed * Time.deltaTime);
	}
}
