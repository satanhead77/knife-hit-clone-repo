﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Zenject;

public class ShootController : BaseController
{
	public event Action ShootIsFired;
	public int numberOfknives;
	[SerializeField] private Transform _shootStartPos;
	[SerializeField] private KeyCode _shootButton;

	[Inject] private StandartKnife.Factory _standartKnife;
	[Inject] private ExplosiveKnife.Factory _explosiveKnife;
	[Inject] private CreatedObject _createdObject;

	public void SetNumberOfKnives(int numberOfKnives)
	{
		numberOfknives = numberOfKnives;
	}

	public override void ControllerUpdate()
	{
		if (_controllerIsEnabked)
		{
			if (Input.GetKeyDown(_shootButton) && numberOfknives > 0)
				KnifeInstantiate();
		}
	}

	private void KnifeInstantiate()
	{
		AbstaractKnife knife;

		if (numberOfknives > 1)
		{
			knife = _standartKnife.Create();
			_createdObject.CreatedObjectSetParent(knife);
			knife.transform.position = _shootStartPos.position;//
		}
	    else if(numberOfknives == 1)
		{
			knife = _explosiveKnife.Create();
			_createdObject.CreatedObjectSetParent(knife);
			knife.transform.position = _shootStartPos.position;//
		}

		numberOfknives--;
		ShootIsFired?.Invoke();
	}
}
