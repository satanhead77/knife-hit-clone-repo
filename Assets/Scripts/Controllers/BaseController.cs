﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseController : MonoBehaviour
{
	protected bool _controllerIsEnabked = true;
	public abstract void ControllerUpdate();
}
