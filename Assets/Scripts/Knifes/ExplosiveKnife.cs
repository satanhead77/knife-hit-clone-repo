﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;

public class ExplosiveKnife : AbstaractKnife
{
	protected override void HitTheTarget()
	{
		_destroyTargetController.DestroyTarget();//временно
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == _targetTag)
		{
			HitTheTarget();
			Destroy(gameObject);
		}
	}

	public class Factory : PlaceholderFactory<ExplosiveKnife>
	{
	}
}
