﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class StandartKnife : AbstaractKnife
{
	private Transform _newParent;

	protected override void HitTheTarget()
	{
		transform.SetParent(_newParent);
		_rigidbody.bodyType = RigidbodyType2D.Static;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == _targetTag)
		{
			_newParent = collision.gameObject.transform;

			Debug.Log("target");
			_isCollisionСhecked = true;

			HitTheTarget();
		}
	}

	public class Factory : PlaceholderFactory<StandartKnife>
	{
	}
}
