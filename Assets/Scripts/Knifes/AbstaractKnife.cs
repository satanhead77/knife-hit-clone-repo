﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;

public abstract class AbstaractKnife : MonoBehaviour
{
	[SerializeField] protected float _speed;
	[SerializeField] protected bool _isCollisionСhecked;
	protected Rigidbody2D _rigidbody;
	protected string _targetTag = "Target";
	protected DestroyTargetController _destroyTargetController; //временно
	[Inject] private LevelInitializeController _levelInitializer;

	private void Start()
	{
		_rigidbody = GetComponent<Rigidbody2D>();
		var obj = GameObject.FindGameObjectWithTag("destroyTarget");//временно
		_destroyTargetController = obj.GetComponent<DestroyTargetController>();//временно
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Knife")
		{
			if (!_isCollisionСhecked)
			{
				HitTheKnife();
				_isCollisionСhecked = true;
			}
		}
	}

	protected void Move()
	{
		transform.position += Vector3.up * _speed * Time.deltaTime;
	}

	private void Update()
	{
		if (!_isCollisionСhecked)
			Move();
	}

	protected void HitTheKnife()
	{
		_rigidbody.gravityScale = 1;
		Debug.Log("HitTheKnife");
		_levelInitializer.StageFailedTrue();
	}

	protected abstract void HitTheTarget();
}
