﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Zenject;

public class DestroyTargetController : MonoBehaviour
{
	public event Action TargetDestroyed;
	public event Action BossDestroyed;
	[SerializeField] private Target _target;
	[SerializeField] private bool _fragmentsEnabled;
	[Inject] LevelInitializeController _levelInitializeController;
	

	public void SetTarget(Target target)
	{
		_target = target;
	}

	public void DestroyTarget()
	{
		_target.RemoveParentFragment();
		Destroy(_target.gameObject);

		if (!_levelInitializeController.IsBossStage)
		{
			TargetDestroyed?.Invoke();
		}
		else if(_levelInitializeController.IsBossStage)
		{
			BossDestroyed?.Invoke();
		}
	}

	public void DestroyTargetWithoutEffects()
	{
		if (_target.gameObject != null)
		{
			Destroy(_target.gameObject);
		}
	}
}
