﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class FruitRandomCreator : MonoBehaviour
{
	[Inject] private FruitDestroy.Factory _fruit;
	[Inject] private LevelInitializeController levelInitializer;
	private float _positionOffset = 0.8f;
	private Vector3 _position;

	private void Start()
	{
		levelInitializer.NewStageCreated += CreateFruit;
		levelInitializer.BossStageCreated += CreateFruitsInACircle;
	}
	private void OnDestroy()
	{
		levelInitializer.NewStageCreated -= CreateFruit;
		levelInitializer.BossStageCreated -= CreateFruitsInACircle;
	}

	private void CreateFruit()
	{
		int randomValue;
		randomValue = Random.Range(0, 4);

		if (randomValue == 1)
		{
			var target = GameObject.FindGameObjectWithTag("Target");

			_position = new Vector3(target.transform.position.x, target.transform.position.y + _positionOffset, target.transform.position.z);

			var createdFruit = _fruit.Create();//

			createdFruit.transform.position = _position;//

			createdFruit.transform.SetParent(target.transform);

			Debug.Log("CreateFruit");
		}
	}

	private Vector3 FruitsCircle(Vector3 center, float radius,int a)
	{
		float ang = a;
		Vector3 pos;
		pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
		pos.y = center.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
		pos.z = center.z;
		return pos;
	}

	private void CreateFruitsInACircle()
	{
		var target = GameObject.FindGameObjectWithTag("Target");
		Vector3 center = target.transform.position;

		for (int i = 0; i < 12; i++)
		{
			int a = i * 30;
			Vector3 position = FruitsCircle(center, 0.7f, a);
			var fruit =  _fruit.Create();
			fruit.transform.position = position;
			fruit.transform.SetParent(target.transform);
		}
	}
}
