﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
	[SerializeField] private List<Fragment> _targetFragments;

	private void OnDestroy()
	{
		//RemoveParentFragment();
		EnableFragments();
	}

	private void EnableFragments()
	{
		foreach(var fragmet in _targetFragments)
		{
			fragmet.gameObject.SetActive(true);
		}
	}

	public void RemoveParentFragment()
	{
		foreach (var fragment in _targetFragments)
		{
			fragment.transform.SetParent(null);
		}
	}
}
