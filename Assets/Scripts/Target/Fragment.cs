﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fragment : MonoBehaviour
{
	[SerializeField] private Rigidbody2D _rigidbody;
	[SerializeField] private Vector2 _forceVector;
	[SerializeField] private float _upForce;
	[SerializeField] private float _torqueForce;
	[SerializeField] private bool _fragmentsEnabled;

	private void OnEnable()
	{
		_fragmentsEnabled = true;
	}

	private void Update()
	{
		Destroy(gameObject,3);
	}

	private void FixedUpdate()
	{
		FragmentAddForce();
	}

	private void FragmentAddForce()
	{
		if (_fragmentsEnabled)
		{
			_rigidbody.AddForce(_forceVector * _upForce, ForceMode2D.Force);
			_rigidbody.AddTorque(_torqueForce, ForceMode2D.Impulse);
			_fragmentsEnabled = false;
		}
	}
}
