﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Zenject;

public class FruitsCountUi : MonoBehaviour
{
	[SerializeField] private TextMeshProUGUI _fruitsCountText;
	[Inject] private FruitsCount fruitsCount;

	private void Start()
	{
		fruitsCount.FruitAdded += FruitsCountViewUpdate;
	}

	private void OnDestroy()
	{
		fruitsCount.FruitAdded -= FruitsCountViewUpdate;
	}

	private void FruitsCountViewUpdate()
	{
		_fruitsCountText.text = fruitsCount.fruitsCount.ToString();
	}
}
