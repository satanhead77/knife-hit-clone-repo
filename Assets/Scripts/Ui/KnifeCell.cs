﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KnifeCell : MonoBehaviour
{
	[SerializeField] private Image _knifeImage;

	public void EnableOrDisableKnifeImage(bool isEnabledKnifeImage)
	{
		_knifeImage.enabled = isEnabledKnifeImage;
	}
}
