﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using TMPro;

public class KnivesCountUi : MonoBehaviour
{
	[SerializeField] private Transform _grid;
	[SerializeField] private KnifeCell _knifeCellPrefab;
	[SerializeField] private List<KnifeCell> _knifesCellsWithEnabledImage;
	[SerializeField] private List<KnifeCell> cellsToDestroy;
	[SerializeField] private GameSettings _gameSettings;
	[Inject] private ShootController _shootController;
	[Inject] private LevelInitializeController _levelInitializer;

	private void Start()
	{
		cellsToDestroy = new List<KnifeCell>();
		CreateKnifeCellsOnStart();
		_shootController.ShootIsFired += DisableKnifeCellImage;
		_levelInitializer.NewStageCreated += CreateKnifeCells;
	}
	private void OnDestroy()
	{
		_shootController.ShootIsFired -= DisableKnifeCellImage;
		_levelInitializer.NewStageCreated -= CreateKnifeCells;
	}

	private void CreateKnifeCellsOnStart()
	{
		for (int i = 0; i < _gameSettings.GetStartNumberOfKnives(); ++i)
		{
			var newPrefab = Instantiate(_knifeCellPrefab, _grid.transform);
			_knifesCellsWithEnabledImage.Add(newPrefab);
		}
		_knifesCellsWithEnabledImage.Reverse();
	}

	private void CreateKnifeCells()
	{
		ClearGridWithCells();

		for (int i = 0; i < _shootController.numberOfknives; ++i)
		{
			var newPrefab = Instantiate(_knifeCellPrefab, _grid.transform);
			_knifesCellsWithEnabledImage.Add(newPrefab);
		}
		_knifesCellsWithEnabledImage.Reverse();
	}

	private void DisableKnifeCellImage()
	{
		var cell = _knifesCellsWithEnabledImage[0];
		cellsToDestroy.Add(cell);
		cell.EnableOrDisableKnifeImage(false);
		_knifesCellsWithEnabledImage.Remove(cell);
	}

	private void ClearGridWithCells()
	{
		foreach(var cell in _knifesCellsWithEnabledImage)
		{
			cell.gameObject.SetActive(false);
		}

		foreach (var cell in  cellsToDestroy)
		{
			cell.gameObject.SetActive(false);
		}

		cellsToDestroy.Clear();

		_knifesCellsWithEnabledImage.Clear();
	}
}
