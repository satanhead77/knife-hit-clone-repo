﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Zenject;

public class UiMessages : MonoBehaviour
{
	public event Action RestartButtonClicked;
	[SerializeField] private TextMeshProUGUI _nextStageText;
	[SerializeField] private TextMeshProUGUI _failedText;
	[SerializeField] private GameObject _levelCompleteTextAndButton;
	[Inject] private DestroyTargetController _destroyTargetController;
	[Inject] private LevelInitializeController _levelInitializer;

	private void Start()
	{
		DisableNextStageText();
		DisableFaildText();
		DisableLevelCompleteText();

		_destroyTargetController.TargetDestroyed += EnableNextStageText;
		_levelInitializer.NewStageCreated += DisableNextStageText;
		_levelInitializer.StageFailedEvent += EnableFaildText;//
		_levelInitializer.NewStageCreated += DisableFaildText;

		_destroyTargetController.BossDestroyed += EnableLevelCompleteText;
		_levelInitializer.NewStageCreated += DisableLevelCompleteText;
	}

	private void OnDestroy()
	{
		_destroyTargetController.TargetDestroyed -= EnableNextStageText;
		_levelInitializer.NewStageCreated -= DisableNextStageText;
		_levelInitializer.StageFailedEvent -= EnableFaildText;//
		_levelInitializer.NewStageCreated -= DisableFaildText;

		_destroyTargetController.BossDestroyed -= EnableLevelCompleteText;
		_levelInitializer.NewStageCreated -= DisableLevelCompleteText;
	}

	public void RestartButtonClick()
	{
		RestartButtonClicked?.Invoke();
	}

	private void EnableNextStageText()
	{
		_nextStageText.enabled = true;
	}

	private void DisableNextStageText()
	{
		_nextStageText.enabled = false;
	}

	private void EnableFaildText()
	{
		_failedText.enabled = true;
	}

	private void DisableFaildText()
	{
		_failedText.enabled = false;
	}

	private void EnableLevelCompleteText()
	{
		_levelCompleteTextAndButton.SetActive(true);
	}

	private void DisableLevelCompleteText()
	{
		_levelCompleteTextAndButton.SetActive(false);
	}
}
