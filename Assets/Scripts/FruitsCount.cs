﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Zenject;

public class FruitsCount : MonoBehaviour
{
	public event Action FruitAdded;
	[field:SerializeField] public int fruitsCount { get; private set; }
	

	public void AddFruit(int number)
	{
		fruitsCount += number;
		FruitAdded?.Invoke();
	}
}
