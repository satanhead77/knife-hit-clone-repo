﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName = "GameSettings", order = 52)] 
public class GameSettings : ScriptableObject
{
	[SerializeField] private int _startNumberOfKnives;
	[SerializeField] private int _howManyKnivesWillBeAddedForTheCompletedStage;//сколько ножей добавится за пройденный этап
	[SerializeField] private float _targetStartRotateSpeed;
	[SerializeField] private float _howMuchTheRotateSpeedWillIncreaseForTheCompletedStage;//сколько скорости крчения прибавится за пройденный этап
	[SerializeField] private float _howLongDoesTheTargetSpinInOneDirection;// сколько времени крутится цель в одну сторону перед тем как начать крутиться в другую
	[SerializeField] private Target _standartTarget;// самая простая цель, которая будет на первом уровене
	[SerializeField] private List<Target> _targetsList;// список возможных нестандартных целей 
	[SerializeField] private List<Target> _bossesList;// список возможных боссов
	[SerializeField] private int _maxNumberOfStages;//максимальное кол-во этапов до босса

	public int GetStartNumberOfKnives()
	{
		return _startNumberOfKnives;
	}

	public int GetHowManyKnivesWillBeAddedForTheCompletedStage()
	{
		return _howManyKnivesWillBeAddedForTheCompletedStage;
	}

	public float GetTargetStartRotateSpeed()
	{
		return _targetStartRotateSpeed;
	}

	public float GetHowMuchTheRotateSpeedWillIncreaseForTheCompletedStage()
	{
		return _howMuchTheRotateSpeedWillIncreaseForTheCompletedStage;
	}

	public float GetHowLongDoesTheTargetSpinInOneDirection()
	{
		return _howLongDoesTheTargetSpinInOneDirection;
	}

	public Target GetStandartTarget()
	{
		return _standartTarget;
	}

	public Target GetRandomTargetFromList()
	{
		return RandomListElementsSwap(_targetsList);
	}

	public Target GetRandomBossFromList()
	{
		return RandomListElementsSwap(_bossesList);
	}

	public int GetMaxNumberOfStages()
	{
		return _maxNumberOfStages;
	}

	private Target RandomListElementsSwap(List<Target> List)
	{
		System.Random random = new System.Random();

		for (int i = List.Count - 1; i >= 1; i--)
		{
			int j = random.Next(i + 1);
			Target temp = List[j];
			List[j] = List[i];
			List[i] = temp;
		}

		return List[0];
	}

}
