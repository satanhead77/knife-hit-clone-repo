﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetShaking : MonoBehaviour
{
	[SerializeField] private float _shakingSpeed;
	[SerializeField] private bool _isNeedShake;
	private Vector2 _startPos;

	private void Start()
	{
		_startPos = transform.position;
	}

	private void Update()
	{
		Shake();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "Knife")
		{
			_isNeedShake = true;
		}
	}

	private void Shake()
	{
		if (_isNeedShake)
		{
			transform.position = new Vector2(transform.position.x, transform.position.y + 1 * _shakingSpeed * Time.deltaTime);
			StartCoroutine(ShakeEnumerator());
		}
	}

	IEnumerator ShakeEnumerator()
	{
		yield return new WaitForSeconds(0.1f);
		transform.position = _startPos;
		_isNeedShake = false;
	}
}
