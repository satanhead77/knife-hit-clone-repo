using UnityEngine;
using Zenject;
using System.Collections.Generic;

public class Installer : MonoInstaller
{
	[SerializeField] private DestroyTargetController _destroyTargetController;
	[SerializeField] private ShootController _shootController;
	[SerializeField] private CircleRotateController _circleRotateController;
	[SerializeField] private LevelInitializeController _levelInitializeController;
	[SerializeField] private CreatedObject _createdObject;
	[SerializeField] private FruitsCount _fruitsCount;
	[SerializeField] private UiMessages _uiMessages;
	private List<BaseController> _baseControllers;

	[SerializeField] private GameObject _knifePrefab;//
	[SerializeField] private GameObject _explosiveknifePrefab;//
	[SerializeField] private GameObject _fruitPrefab;

	public override void InstallBindings()
	{
		Container.Bind<ShootController>().FromInstance(_shootController).AsSingle();
		Container.Bind<CircleRotateController>().FromInstance(_circleRotateController).AsSingle();
		Container.Bind<DestroyTargetController>().FromInstance(_destroyTargetController).AsSingle();
		Container.Bind<LevelInitializeController>().FromInstance(_levelInitializeController).AsSingle();
		Container.Bind<CreatedObject>().FromInstance(_createdObject).AsSingle();
		Container.Bind<FruitsCount>().FromInstance(_fruitsCount).AsSingle();
		Container.Bind<UiMessages>().FromInstance(_uiMessages).AsSingle();

		Container.BindFactory<StandartKnife, StandartKnife.Factory>().FromComponentInNewPrefab(_knifePrefab);//
		Container.BindFactory<ExplosiveKnife, ExplosiveKnife.Factory>().FromComponentInNewPrefab(_explosiveknifePrefab);//
		Container.BindFactory<FruitDestroy, FruitDestroy.Factory>().FromComponentInNewPrefab(_fruitPrefab);//

		AddControllerTolist();
	}

	private void AddControllerTolist()
	{
		_baseControllers = new List<BaseController>();
		_baseControllers.Add(_shootController);
		_baseControllers.Add(_circleRotateController);
		_baseControllers.Add(_levelInitializeController);
	}

	private void Update()
	{
		foreach(var controller in _baseControllers)
		{
			controller.ControllerUpdate();
		}
	}
}