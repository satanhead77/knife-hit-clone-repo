﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatedObject : MonoBehaviour
{
	[SerializeField] private Transform _createdObjectParent;

	public void CreatedObjectSetParent(MonoBehaviour Obj)
	{
		Obj.transform.SetParent(_createdObjectParent);
	}
}
